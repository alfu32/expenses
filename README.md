# expenses
graphical representation for monthly expenses
## Financial Tool
 graphical projection  and aggregation of monthly costs : loans, expenses, future costs.

Use it to make a projection for your budget in function of recurrent costs and income.

Taking a mortgage or a loan usually has a heavy impact on your budget, besides ... the banks will offer you capital at different interest rates(fixed, not fixed), 
different payment schedules (flat or growing) 
and different time length. 

This tool makes it easy to visualize your projected budget on  a long term
	                         

## install
```
git clone https://github.com/alfu32/expenses.git
cd expenses
bower install
```

## run
either run it with the built-in http-server,
```
npm install
http-server
```
then go to http://localhost/build/

Or just open the file build/index.html in your browser

## save model       : `ctrl-shit-s`

## load saved model : 
drag the previously saved json file from an explorer window and drop it into the page
